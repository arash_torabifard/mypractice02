package com.example.user.javalinklayout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class Main2Activity extends AppCompatActivity {
    TextView fullName;
    ImageView imgCeleb_02;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        fullName = (TextView) findViewById(R.id.fullName);
        imgCeleb_02 = (ImageView) findViewById(R.id.imgCeleb_02);
        fullName.setText(getIntent().getStringExtra("name_family"));
        Picasso.with(Main2Activity.this).load(getIntent().getStringExtra("imgUrl_01")).into(imgCeleb_02);
    }
}
