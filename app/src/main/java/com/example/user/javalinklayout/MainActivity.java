package com.example.user.javalinklayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {
    TextView result;
    EditText lName;
    EditText fName;
    Button btnSubmit;
    ImageView imgCeleb_01;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        result = (TextView) findViewById(R.id.result);
        lName = (EditText) findViewById(R.id.lName);
        fName = (EditText) findViewById(R.id.fName);
        imgCeleb_01 = (ImageView) findViewById(R.id.imgCeleb_01);
        lName.setText(getSharedPref("lName",""));
        fName.setText(getSharedPref("fName",""));

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String output = fName.getText() + " " + lName.getText();
                result.setText(output);
                Toast.makeText(MainActivity.this, "Submitted", Toast.LENGTH_LONG).show();
                setSharedPref("fName",fName.getText().toString());
                setSharedPref("lName",lName.getText().toString());
            }
        });
        Picasso.with(MainActivity.this).
                load("http://allswalls.com/images/edge-of-tomorrow-action-scifi-warrior-11-wallpaper-1.jpg").into(imgCeleb_01);

        findViewById(R.id.imgCeleb_01).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSharedPref("fName",fName.getText().toString());
                setSharedPref("lName",lName.getText().toString());
                Intent secondActivity = new Intent(MainActivity.this, Main2Activity.class);
                String name = fName.getText().toString();
                String familyName = lName.getText().toString();
                secondActivity.putExtra("name_family", name + " " + familyName);
                secondActivity.putExtra("imgUrl_01", "http://allswalls.com/images/edge-of-tomorrow-action-scifi-warrior-11-wallpaper-1.jpg");
                startActivity(secondActivity);

            }
        });


    }
    private void setSharedPref(String key, String value){
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString(key,value).commit();
    }
    private String getSharedPref(String key,String defaultValue){
       return PreferenceManager.getDefaultSharedPreferences(this).getString(key,defaultValue);
    }
}
